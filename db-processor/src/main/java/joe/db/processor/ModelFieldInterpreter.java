/* 
 * The MIT License
 *
 * Copyright 2017 joe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package joe.db.processor;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.Category;
import com.google.appengine.api.datastore.Email;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.GeoPt;
import com.google.appengine.api.datastore.IMHandle;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Link;
import com.google.appengine.api.datastore.PhoneNumber;
import com.google.appengine.api.datastore.PostalAddress;
import com.google.appengine.api.datastore.Rating;
import com.google.appengine.api.datastore.ShortBlob;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.users.User;
import com.google.common.collect.ImmutableSet;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import joe.db.Model;
import joe.db.Record;

class ModelFieldInterpreter extends ModelAttributeInterpreter {
  private static final ImmutableSet<String> MAPPED_TYPES = ImmutableSet.of(BigDecimal.class.getCanonicalName(),
                                                                           Blob.class.getCanonicalName(),
                                                                           BlobKey.class.getCanonicalName(),
                                                                           Boolean.class.getCanonicalName(),
                                                                           Category.class.getCanonicalName(),
                                                                           Date.class.getCanonicalName(),
                                                                           Double.class.getCanonicalName(),
                                                                           Email.class.getCanonicalName(),
                                                                           EmbeddedEntity.class.getCanonicalName(),
                                                                           GeoPt.class.getCanonicalName(),
                                                                           IMHandle.class.getCanonicalName(),
                                                                           Key.class.getCanonicalName(),
                                                                           Link.class.getCanonicalName(),
                                                                           List.class.getCanonicalName(),
                                                                           Long.class.getCanonicalName(),
                                                                           PhoneNumber.class.getCanonicalName(),
                                                                           PostalAddress.class.getCanonicalName(),
                                                                           Rating.class.getCanonicalName(),
                                                                           ShortBlob.class.getCanonicalName(),
                                                                           String.class.getCanonicalName(),
                                                                           Text.class.getCanonicalName(),
                                                                           User.class.getCanonicalName());
  private final ImmutableSet<String> supportedTypes;

  ModelFieldInterpreter(final ProcessingEnvironment environment) { this(environment, MAPPED_TYPES); }

  ModelFieldInterpreter(final ProcessingEnvironment environment, final ImmutableSet<String> interpreterMappedTypes) {
    super(environment);
    supportedTypes = interpreterMappedTypes;
  }

  MetaField read(final VariableElement variable) {
    final String type = fieldTypeName(variable);

    checkModifiersOf(variable);
    checkAnnotationsOf(variable, type);

    return new MetaField(type,
                         nameOf(variable),
                         descriptionOf(variable),
                         propertyOf(variable),
                         isIndexed(variable),
                         isRequired(variable),
                         modifiersOf(variable),
                         constraintsOf(variable));
  }

  String fieldTypeName(final VariableElement variable) {
    final TypeMirror varType = variable.asType();

    if (varType.getKind().isPrimitive()) {
      throw new IllegalFieldTypeException(variable, "Primitive types aren't supported.");
    }

    final String type = typeNameOf(variable);
    if (!supportedTypes.contains(type)) {
      throw new IllegalFieldTypeException(variable, type, supportedTypes);
    }
    return type;
  }

  void checkModifiersOf(final VariableElement variable) throws ModelException {
    final Set<Modifier> modifiers = variable.getModifiers();
    if (modifiers.contains(Modifier.STATIC)) {
      throw new ModelException(variable, "only member fields can be annotated as properties");
    }
    if (modifiers.contains(Modifier.TRANSIENT)) {
      throw new ModelException(variable, "only member fields can be annotated as properties");
    }
    if (modifiers.contains(Modifier.VOLATILE)) {
      throw new ModelException(variable, "only member fields can be annotated as properties");
    }
  }

  void checkAnnotationsOf(final VariableElement variable, final String typeCanonicalName) {
    if (variable.getAnnotation(Record.id.class) != null) {
      throw new ModelException(variable, "@Id fields can not be interpreted as @Property");
    }
    if (variable.getAnnotation(Record.parent.class) != null) {
      throw new ModelException(variable, "@Parent fields can not be interpreted as @Property");
    }
    if (variable.getAnnotation(Record.indexed.class) != null) {
      if (Objects.equals(Text.class.getCanonicalName(), typeCanonicalName)) {
        throw new ModelException(variable, "Text fields can't be @Indexed.");
      }
      if (Objects.equals(Blob.class.getCanonicalName(), typeCanonicalName)) {
        throw new ModelException(variable, "Blob fields can't be @Indexed.");
      }
      if (Objects.equals(EmbeddedEntity.class.getCanonicalName(), typeCanonicalName)) {
        throw new ModelException(variable, "EmbeddedEntity fields can't be @Indexed.");
      }
    }
  }

  String propertyOf(final VariableElement variable) {
    final Record.property property = variable.getAnnotation(Record.property.class);
    if (property == null || "".equals(property.value())) {
      return variable.getSimpleName().toString();
    }
    final String propertyName = property.value();
    if ("".equals(propertyName)) {
      return variable.getSimpleName().toString();
    } else {
      return propertyName;
    }
  }

  boolean isReference(final Element clazz) {
    final Model modelAnnotation = clazz.getAnnotation(Model.class);
    return (modelAnnotation != null);
  }

  boolean isEnum(final Element clazz) { return clazz.getKind() == ElementKind.ENUM; }

  boolean isIndexed(final VariableElement variable) { return variable.getAnnotation(Record.indexed.class) != null; }
}