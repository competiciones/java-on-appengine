/*
 * The MIT License
 *
 * Copyright 2017 java on appengine.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package joe.db.processor;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.UnmodifiableIterator;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeSpec;
import java.util.LinkedList;
import java.util.List;
import javax.lang.model.element.Modifier;

abstract class AttributeGenerator {
  static final Joiner JOINER = Joiner.on('.');

  final ClassName modelClass;
  
  AttributeGenerator(final ClassName modelClassName) { modelClass = modelClassName; }

  Modifier[] attributeModifiers(final MetaModelAttribute attr) {
    return attr.modifiers.toArray(new Modifier[0]);
  }

  abstract void buildAt(TypeSpec.Builder modelSpec);

  abstract String canonicalName();

  static String simpleClassName(final ClassName fieldClassName) { return JOINER.join(fieldClassName.simpleNames()); }
  
  String constraints(final MetaModelAttribute attr) {
    final UnmodifiableIterator<MetaConstraint> iter = attr.constraints.iterator();
    if (iter.hasNext()) {
      final StringBuilder constraints = new StringBuilder();
      constraints.append(iter.next().construtionExpr);
      while (iter.hasNext()) {
        constraints.append(", ").append(iter.next().construtionExpr);
      }
      return constraints.toString();
    } else {
      return "$L";
    }
  }
  
  private static final List<Object> NO_ARGS = ImmutableList.of("noConstraints()");
  List<Object> constraintsArgs(final MetaModelAttribute attr) {
    if (attr.hasConstraints()) {
      final LinkedList<Object> args = new LinkedList<>();
      for (final MetaConstraint constraint : attr.constraints) {
        for (final Object arg : constraint.args) {
          args.add(arg);
        }
      }
      return args;
    } else {
      return NO_ARGS;
    }
  }
}

