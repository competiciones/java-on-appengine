/* 
 * The MIT License
 *
 * Copyright 2017 joe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package joe.db.processor;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.users.User;
import com.google.common.collect.ImmutableMap;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.TypeSpec;
import java.util.Date;
import java.util.LinkedList;
import joe.db.StringField;
import joe.db.BlobKeyField;
import joe.db.BlobField;
import joe.db.BooleanField;
import joe.db.CategoryField;
import joe.db.DateField;
import joe.db.DoubleField;
import joe.db.EmailField;
import joe.db.PhoneNumberField;
import joe.db.EmbeddedEntityListField;
import joe.db.GeoPtField;
import joe.db.IMHandleField;
import joe.db.KeyField;
import joe.db.LinkField;
import joe.db.LongField;
import joe.db.PostalAddressField;
import joe.db.RatingField;
import joe.db.ShortBlobField;
import joe.db.TextField;
import joe.db.UserField;

final class FieldGenerator extends AttributeGenerator {
  private static final ImmutableMap<String, ClassName> INDEXED_BY_TYPE_NAME
      = ImmutableMap.<String, ClassName>builder()
          .put(BlobKey.class.getCanonicalName(), ClassName.get(BlobKeyField.Indexed.class))
          .put(Boolean.class.getCanonicalName(), ClassName.get(BooleanField.Indexed.class))
          .put(Category.class.getCanonicalName(), ClassName.get(CategoryField.Indexed.class))
          .put(Date.class.getCanonicalName(), ClassName.get(DateField.Indexed.class))
          .put(Double.class.getCanonicalName(), ClassName.get(DoubleField.Indexed.class))
          .put(Email.class.getCanonicalName(), ClassName.get(EmailField.Indexed.class))
          .put(GeoPt.class.getCanonicalName(), ClassName.get(GeoPtField.Indexed.class))
          .put(IMHandle.class.getCanonicalName(), ClassName.get(IMHandleField.Indexed.class))
          .put(Key.class.getCanonicalName(), ClassName.get(KeyField.Indexed.class))
          .put(Link.class.getCanonicalName(), ClassName.get(LinkField.Indexed.class))
          .put(Long.class.getCanonicalName(), ClassName.get(LongField.Indexed.class))
          .put(PhoneNumber.class.getCanonicalName(), ClassName.get(PhoneNumberField.Indexed.class))
          .put(PostalAddress.class.getCanonicalName(), ClassName.get(PostalAddressField.Indexed.class))
          .put(Rating.class.getCanonicalName(), ClassName.get(RatingField.Indexed.class))
          .put(ShortBlob.class.getCanonicalName(), ClassName.get(ShortBlobField.Indexed.class))
          .put(String.class.getCanonicalName(), ClassName.get(StringField.Indexed.class))
          .put(User.class.getCanonicalName(), ClassName.get(UserField.Indexed.class))
          .build();

  private static final ImmutableMap<String, ClassName> UNINDEXED_BY_TYPE_NAME
      = ImmutableMap.<String, ClassName>builder()
          .put(Blob.class.getCanonicalName(), ClassName.get(BlobField.class))
          .put(BlobKey.class.getCanonicalName(), ClassName.get(BlobKeyField.Unindexed.class))
          .put(Boolean.class.getCanonicalName(), ClassName.get(BooleanField.Unindexed.class))
          .put(Category.class.getCanonicalName(), ClassName.get(CategoryField.Unindexed.class))
          .put(Date.class.getCanonicalName(), ClassName.get(DateField.Unindexed.class))
          .put(Double.class.getCanonicalName(), ClassName.get(DoubleField.Unindexed.class))
          .put(Email.class.getCanonicalName(), ClassName.get(EmailField.Unindexed.class))
          .put(EmbeddedEntity.class.getCanonicalName(), ClassName.get(EmbeddedEntityListField.class))
          .put(GeoPt.class.getCanonicalName(), ClassName.get(GeoPtField.Unindexed.class))
          .put(IMHandle.class.getCanonicalName(), ClassName.get(IMHandleField.Unindexed.class))
          .put(Key.class.getCanonicalName(), ClassName.get(KeyField.Unindexed.class))
          .put(Link.class.getCanonicalName(), ClassName.get(LinkField.Unindexed.class))
          .put(Long.class.getCanonicalName(), ClassName.get(LongField.Unindexed.class))
          .put(PhoneNumber.class.getCanonicalName(), ClassName.get(PhoneNumberField.Unindexed.class))
          .put(PostalAddress.class.getCanonicalName(), ClassName.get(PostalAddressField.Unindexed.class))
          .put(Rating.class.getCanonicalName(), ClassName.get(RatingField.Unindexed.class))
          .put(ShortBlob.class.getCanonicalName(), ClassName.get(ShortBlobField.Unindexed.class))
          .put(String.class.getCanonicalName(), ClassName.get(StringField.Unindexed.class))
          .put(Text.class.getCanonicalName(), ClassName.get(TextField.class))
          .put(User.class.getCanonicalName(), ClassName.get(UserField.Unindexed.class))
          .build();

  private final ClassName fieldClass;
  final MetaField field;

  FieldGenerator(final MetaField field, final ClassName modelClass) {
    super(modelClass);
    this.fieldClass = mappedFieldClass(field);
    this.field = field;
  }

  @Override final String canonicalName() { return field.canonicalNameAt(modelClass.toString()); }
  
  @Override
  void buildAt(final TypeSpec.Builder modelSpec) {
    modelSpec.addField(
        FieldSpec.builder(fieldClass, field.name, attributeModifiers(field))
            .initializer(fieldInitializerFormat(), fieldInitializerArgs())
            .build()
    );
  }

  protected String fieldInitializerFormat() {
    return "new $T(canonicalName($S), description($S), property($S), field($S), required($L), jsonName($S), jsonPath($S), "+constraints(field)+')';
  }

  private Object[] fieldInitializerArgs() {
    final LinkedList<Object> args = new LinkedList<>();
    args.add(fieldClass);
    args.add(canonicalName());
    args.add(field.description);
    args.add(field.property);
    args.add(field.name);
    args.add(field.required);
    args.add(field.name);
    args.add(field.name);
    args.addAll(constraintsArgs(field));
    return args.toArray();
  }

  ClassName mappedFieldClass(final MetaField field) {
    if (field.indexed) {
      return getFieldClass(field, INDEXED_BY_TYPE_NAME);
    } else {
      return getFieldClass(field, UNINDEXED_BY_TYPE_NAME);
    }
  }

  ClassName getFieldClass(final MetaField field, final ImmutableMap<String, ClassName> fieldClasses) {
    if (!fieldClasses.containsKey(field.type)) {
      throw new ModelException("No Field known for type [" + field.type + "] used at '" + field.name + "'.");
    }
    return fieldClasses.get(field.type);
  }
}