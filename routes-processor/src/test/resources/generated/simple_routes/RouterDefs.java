package processor.test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import entrename.gyms.List;
import entrename.gyms.Show;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;
import java.util.regex.Pattern;
import javax.annotation.Generated;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import joe.web.route.ParameterizedRoute;
import joe.web.route.Route;
import joe.web.route.RouterServlet;

@Generated(
    value = "joe-routes",
    comments = "Build from specs at: src/test/resources/routes/simple_routes.csv",
    date = "2017-02-23"
)
abstract class RouterDefs extends RouterServlet {
  private static final long serialVersionUID = 1487818800000L;

  private final ParameterizedRoute entrename_gyms_Show_route = new ParameterizedRoute("/gyms/{id: [0-9]+}", Pattern.compile("/gyms/(?<p0>[0-9]+)"), ImmutableList.of("id"));

  private final Route entrename_gyms_List_route = new Route("/gyms");

  @Override
  public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws
      ServletException, IOException {
    final ImmutableMap.Builder<String, String> routeParameters = ImmutableMap.builder();
    if (entrename_gyms_Show_route.matches(request, routeParameters)) {
      run(new Show(request, response, routeParameters.build()));
      return;
    }
    if (entrename_gyms_List_route.matches(request)) {
      run(new List(request, response));
      return;
    }
    unhandledGet(request, response);
  }
}