package processor.test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import entrename.gyms.Create;
import entrename.gyms.Destroy;
import entrename.gyms.List;
import entrename.gyms.Show;
import entrename.gyms.Update;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;
import java.util.regex.Pattern;
import javax.annotation.Generated;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import joe.web.route.ParameterizedRoute;
import joe.web.route.Route;
import joe.web.route.RouterServlet;

@Generated(
    value = "joe-routes",
    comments = "Build from specs at: src/test/resources/routes/multiple_verb_and_package.csv",
    date = "2017-02-23"
)
abstract class RouterDefs extends RouterServlet {
  private static final long serialVersionUID = 1487818800000L;

  private final ParameterizedRoute entrename_gyms_Show_route = new ParameterizedRoute("/gyms/{id: [0-9]+}", Pattern.compile("/gyms/(?<p0>[0-9]+)"), ImmutableList.of("id"));

  private final Route entrename_gyms_List_route = new Route("/gyms");

  private final ParameterizedRoute entrename_trainees_Show_route = new ParameterizedRoute("/trainees/{id: [0-9]+}", Pattern.compile("/trainees/(?<p0>[0-9]+)"), ImmutableList.of("id"));

  private final Route entrename_trainees_List_route = new Route("/trainees");

  private final Route entrename_gyms_Create_route = new Route("/gyms");

  private final Route entrename_trainees_Create_route = new Route("/trainees");

  private final ParameterizedRoute entrename_gyms_Update_route = new ParameterizedRoute("/gyms/{id: :digit:+}", Pattern.compile("/gyms/(?<p0>\\p{Digit}+)"), ImmutableList.of("id"));

  private final ParameterizedRoute entrename_trainees_Update_route = new ParameterizedRoute("/trainees/{id: :digit:+}", Pattern.compile("/trainees/(?<p0>\\p{Digit}+)"), ImmutableList.of("id"));

  private final ParameterizedRoute entrename_gyms_Destroy_route = new ParameterizedRoute("/gyms/{id: [0-9]+}", Pattern.compile("/gyms/(?<p0>[0-9]+)"), ImmutableList.of("id"));

  private final ParameterizedRoute entrename_trainees_Destroy_route = new ParameterizedRoute("/trainees/{id: [0-9]+}", Pattern.compile("/trainees/(?<p0>[0-9]+)"), ImmutableList.of("id"));

  @Override
  public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws
      ServletException, IOException {
    final ImmutableMap.Builder<String, String> routeParameters = ImmutableMap.builder();
    if (entrename_gyms_Show_route.matches(request, routeParameters)) {
      run(new Show(request, response, routeParameters.build()));
      return;
    }
    if (entrename_gyms_List_route.matches(request)) {
      run(new List(request, response));
      return;
    }
    if (entrename_trainees_Show_route.matches(request, routeParameters)) {
      run(new entrename.trainees.Show(request, response, routeParameters.build()));
      return;
    }
    if (entrename_trainees_List_route.matches(request)) {
      run(new entrename.trainees.List(request, response));
      return;
    }
    unhandledGet(request, response);
  }

  @Override
  public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws
      ServletException, IOException {
    if (entrename_gyms_Create_route.matches(request)) {
      run(new Create(request, response));
      return;
    }
    if (entrename_trainees_Create_route.matches(request)) {
      run(new entrename.trainees.Create(request, response));
      return;
    }
    unhandledPost(request, response);
  }

  @Override
  public void doPut(final HttpServletRequest request, final HttpServletResponse response) throws
      ServletException, IOException {
    final ImmutableMap.Builder<String, String> routeParameters = ImmutableMap.builder();
    if (entrename_gyms_Update_route.matches(request, routeParameters)) {
      run(new Update(request, response, routeParameters.build()));
      return;
    }
    if (entrename_trainees_Update_route.matches(request, routeParameters)) {
      run(new entrename.trainees.Update(request, response, routeParameters.build()));
      return;
    }
    unhandledPut(request, response);
  }

  @Override
  public void doDelete(final HttpServletRequest request, final HttpServletResponse response) throws
      ServletException, IOException {
    final ImmutableMap.Builder<String, String> routeParameters = ImmutableMap.builder();
    if (entrename_gyms_Destroy_route.matches(request, routeParameters)) {
      run(new Destroy(request, response, routeParameters.build()));
      return;
    }
    if (entrename_trainees_Destroy_route.matches(request, routeParameters)) {
      run(new entrename.trainees.Destroy(request, response, routeParameters.build()));
      return;
    }
    unhandledDelete(request, response);
  }
}