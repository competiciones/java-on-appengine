/*
 * The MIT License
 *
 * Copyright 2017 java on appengine.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package joe.db;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

public class CuitConstraintTest {
  @Test public void when_format_is_illegal_then_value_should_be_considered_invalid() {
    // given values with bad format
    final String[] illegalFormats = {
      "20254697142",
      "20-25469714-",
      "20-25469714-a",
      "20-254697142",
      "2025469714-2",
      "2f-25469714-2",
      "20-254f9714-2"
    };
    
    for (final String valueWithIllegalFormat : illegalFormats) {
      // when, is checked if invalid
      final boolean isInvalid = CuitConstraint.INSTANCE.isInvalid(valueWithIllegalFormat);
      // expect the value is considered illegal and rejected
      assertThat(isInvalid).isTrue();
    }
  }

  @Test public void when_format_is_valid_then_it_should_accept_only_cases_where_verifier_digit_is_correct() {
    // given, a legal value
    final String legalFormatWithCorrectVerifierDigit = "20-25469714-2";
    
    // when, is checked if invalid
    final boolean isInvalid = CuitConstraint.INSTANCE.isInvalid(legalFormatWithCorrectVerifierDigit);
    
    // expect the value is considered legal and accepted
    assertThat(isInvalid).isFalse();
  }
  
  @Test public void when_format_is_valid_then_it_should_reject_cases_where_verifier_digit_isnt_correct() {
    // given, values with incorrect verifiers
    final String[] wrongVerifiers = {
      "20-25469714-1",
      "20-25469714-3",
      "20-25469714-4",
      "20-25469714-5",
      "20-25469714-6",
      "20-25469714-7",
      "20-25469714-8",
      "20-25469714-9",
      "20-25469714-0"
    };

    for (final String valueWithWrongVerifier : wrongVerifiers) {
      // when, is checked if invalid
      final boolean isInvalid = CuitConstraint.INSTANCE.isInvalid(valueWithWrongVerifier);
      // expect the value is considered illegal and rejected
      assertThat(isInvalid).isTrue();
    }
  }
}