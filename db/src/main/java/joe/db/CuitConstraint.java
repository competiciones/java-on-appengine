/*
 * The MIT License
 *
 * Copyright 2017 java on appengine.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package joe.db;

import java.util.regex.Pattern;

public enum CuitConstraint implements Constraint<String> {
  INSTANCE;
  
  private static final Pattern CUIT_PATTERN = Pattern.compile("^\\d{2}-\\d{8}-\\d$");
  private static final int SEEDS[] = {5, 4, 3, 2, 7, 6, 5, 4, 3, 2};

  private static int asInt(final char c) { return c - '0'; }

  private static char lastChar(final String s) { return s.charAt(s.length() - 1); }

  @Override public boolean isInvalid(final String value) {
    if (CUIT_PATTERN.matcher(value).matches()) {
      final int expectedResult = asInt(lastChar(value));
      final String cuitDigits = value.replace("-", "").substring(0, 10);
      int result = 0;
      for (int i = 0; i < SEEDS.length; i++) {
        result += (asInt(cuitDigits.charAt(i)) * SEEDS[i]);
      }
      result = 11 - (result - ((result / 11) * 11));
      if (result == 11) {
        result = 0;
      } else if (result == 10) {
        result = 9;
      }
      return result != expectedResult;
    }
    return true;
  }
  
  @Override public String messageFor(final Attr attr, final String value) { return attr.description()+" tiene q ser un CUIT o CUIL valido"; }
  
  @Override public String getName() { return "cuit"; }
}
