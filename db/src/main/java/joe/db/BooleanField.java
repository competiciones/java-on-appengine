/*
 * The MIT License
 *
 * Copyright 2017 java on appengine.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package joe.db;

import argo.jdom.JsonNode;
import argo.jdom.JsonStringNode;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;

public interface BooleanField extends ScalarField<Boolean> {
  interface Filter {
    Query.FilterPredicate isTrue();

    Query.FilterPredicate isFalse();
  }
  
  @Override default Class<Boolean> type() { return Boolean.class; }

  @Override default JsonNode makeJsonValue(final Boolean value) {
    return BooleanJsonSerializer.INSTANCE.toJson(value);
  }

  @Override default Boolean interpretJson(final JsonNode json) {
    if (json == null) {
      throw new NullPointerException("json");
    }
    return BooleanJsonSerializer.INSTANCE.fromJson(json, jsonPath());
  }

  final class Unindexed extends ScalarField.Unindexed<Boolean> implements BooleanField {
    public Unindexed(final String canonicalName,
                     final String description,
                     final String property,
                     final String field,
                     final boolean required,
                     final JsonStringNode jsonName,
                     final String jsonPath,
                     final Constraint... constraints) {
      super(canonicalName, description, property, field, required, jsonName, jsonPath, constraints);
    }
  }

  final class Indexed extends ScalarField.Indexed<Boolean> implements BooleanField, BooleanField.Filter {
    private final Query.FilterPredicate isTrue;
    private final Query.FilterPredicate isFalse;

    public Indexed(final String canonicalName,
                   final String description,
                   final String property,
                   final String field,
                   final boolean required,
                   final JsonStringNode jsonName,
                   final String jsonPath,
                   final Constraint... constraints) {
      super(canonicalName, description, property, field, required, jsonName, jsonPath, new PropertyProjection(property, Boolean.class), constraints);
      this.isTrue = new Query.FilterPredicate(property, Query.FilterOperator.EQUAL, Boolean.TRUE);
      this.isFalse = new Query.FilterPredicate(property, Query.FilterOperator.EQUAL, Boolean.FALSE);
    }
  
    @Override public Query.FilterPredicate isTrue() { return isTrue; }

    @Override public Query.FilterPredicate isFalse() { return isFalse; }
  }
}