/* 
 * The MIT License
 *
 * Copyright 2017 joe.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package joe.db;

import com.google.appengine.api.datastore.FetchOptions;

public abstract class RootActiveEntity
    extends ActiveEntity {
  /**
   * Constructs a ROOT active entity defining its kind.
   * @param kind Kind of the active entity.
   */
  protected RootActiveEntity(final String kind) { super(kind); }

  /* **************************************************************************
   * query building facilities
   */
  public final SelectEntities selectAll() {
    return new SelectEntities(makeQuery(), FetchOptions.Builder.withDefaults());
  }
  public final SelectEntities selectKeys() {
    return new SelectEntities(makeQuery().setKeysOnly(), FetchOptions.Builder.withDefaults());
  }
  public final SelectEntities select(final Filterable<?>... projectedProperties) {
    return new SelectEntities(projection(projectedProperties), FetchOptions.Builder.withDefaults());
  }
  public final SelectEntities select(final Iterable<Filterable<?>> projectedProperties) {
    return new SelectEntities(projection(projectedProperties), FetchOptions.Builder.withDefaults());
  }
}
