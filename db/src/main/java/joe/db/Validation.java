/*
 * The MIT License
 *
 * Copyright 2017 java on appengine.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package joe.db;

import argo.jdom.JsonField;
import argo.jdom.JsonNode;
import argo.jdom.JsonNodeFactories;
import argo.jdom.JsonStringNode;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;

public class Validation {
  private final ArrayListMultimap<Attr, String> errors;

  public Validation() {
    this.errors = ArrayListMultimap.create();
  }

  public boolean success() { return errors.isEmpty(); }

  public boolean failure() { return !errors.isEmpty(); }

  public List<String> get(final Attr attr) {
    if (errors.containsKey(attr)) {
      return errors.get(attr);
    } else {
      return ImmutableList.of();
    }
  }

  public JsonNode asJson() {
    final ArrayList<JsonField> jsonFields = new ArrayList<>(errors.size());
    for (final Attr attr : errors.keySet()) {
      final List<String> messages = errors.get(attr);
      switch (messages.size()) {
        case 0:
          break;
        case 1:
          jsonFields.add(JsonNodeFactories.field(attr.jsonName(), JsonNodeFactories.string(messages.get(0))));
          break;
        default: {
          final ArrayList<JsonStringNode> jsonMsgs = new ArrayList<>(messages.size());
          for (final String msg : messages) {
            jsonMsgs.add(JsonNodeFactories.string(msg));
          }
          jsonFields.add(JsonNodeFactories.field(attr.jsonName(), JsonNodeFactories.array(jsonMsgs)));
        }
      }
    }
    return JsonNodeFactories.object(jsonFields);
  }

  public void reject(final Attr attr, final String message) {
    if (attr == null) {
      throw new NullPointerException("attr");
    }
    if (message == null) {
      throw new NullPointerException("message");
    }
    errors.put(attr, message);
  }
}
